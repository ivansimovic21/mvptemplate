package ivan.ba.mvptemplate

import android.app.Application
import android.content.Context
import ivan.ba.mvptemplate.inject.*
import ivan.ba.mvptemplate.view.areaPost.PostModule
import ivan.ba.mvptemplate.view.login.LoginModule
import javax.inject.Inject

/**
 * Created by isimovic on 30.01.2018..
 */
class MyApp : Application() {
    companion object {
        //platformStatic allow access it from java code
        @JvmStatic
        lateinit var graph: ApplicationComponent
        lateinit var graphRepository: RepositoryComponent
        lateinit var instance: MyApp

    }

    fun getContext(): Context {
        return applicationContext
    }

    init {
        instance = this
    }

    @Inject
    override fun onCreate() {
        super.onCreate()
        graph = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .loginModule(LoginModule())
                .postModule(PostModule())
                .build()

        graphRepository = DaggerRepositoryComponent.builder().repositoryModule(RepositoryModule(this)).build()
    }
}