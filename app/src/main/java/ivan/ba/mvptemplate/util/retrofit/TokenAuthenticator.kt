package ivan.ba.mvptemplate.util.retrofit

import ivan.ba.mvptemplate.data.datamodels.DMAccessToken
import ivan.ba.mvptemplate.data.retrofit.APIUser
import ivan.ba.mvptemplate.util.options.MyOptions
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Call
import retrofit2.Retrofit
import java.io.IOException
import java.net.Proxy

/**
 * Created by isimovic on 18.01.2018..
 */
class TokenAuthenticator(val retrofitForToken: Retrofit) : Authenticator {
    override fun authenticate(route: Route?, response: Response?): Request? {
        val clientForToken = retrofitForToken.create(APIUser::class.java)
        val call: Call<DMAccessToken> = clientForToken.RefreshToken("refresh_token", MyOptions.RefreshToken.GET()!!)
        try {
            val tokenResponse: retrofit2.Response<DMAccessToken> = call.execute()
            if (tokenResponse.isSuccessful) {
                MyOptions.Token.SET(tokenResponse.body()!!.access_token)
                MyOptions.RefreshToken.SET(tokenResponse.body()!!.refresh_token)
                //njectionRetrofit.provideRetrofit(new = true)
            } else {
                return null
            }
        } catch (e: Exception) {
            return null
        }
        // Add new header to rejected request and retry it
        return response!!.request().newBuilder()
                .header("Authorization", "Bearer " + MyOptions.Token.GET())
                .build()
    }

    @Throws(IOException::class)
    fun authenticateProxy(proxy: Proxy, response: Response): Request? {
        // Null indicates no attempt to authenticate.
        return null
    }
}