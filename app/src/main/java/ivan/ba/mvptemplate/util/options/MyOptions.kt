package ivan.ba.mvptemplate.util.options

import ivan.ba.mvptemplate.data.datamodels.DMLoggedUser


/**
 * Created by isimovic on 08.01.2018..
 */
object MyOptions {
    val User: Option<DMLoggedUser> = Option ("User","",DMLoggedUser::class.java)
    val Online:Option<Boolean> = Option ("Online","false",Boolean::class.java)
    val Firebase:Option<Boolean> = Option ("Firebase","true",Boolean::class.java)
    val Token:Option<String> = Option ("Token","",String::class.java)
    val RefreshToken:Option<String> = Option ("RefreshToken","",String::class.java)
    val IP = "http://192.168.10.158:58081/"
}
