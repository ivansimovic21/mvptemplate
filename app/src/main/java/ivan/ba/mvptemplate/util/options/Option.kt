package ivan.ba.mvptemplate.util.options

import android.content.Context
import ivan.ba.mvptemplate.MyApp
import ivan.ba.mvptemplate.util.MyGson
import java.io.Serializable

/**
 * Created by isimovic on 08.01.2018..
 */
class Option<T>(var Name: String, var Default: String, var Type:Class<T>) : Serializable {

    fun GET(): T? {
        val preferences = MyApp.instance.getContext().getSharedPreferences("MyApp", Context.MODE_PRIVATE)
        val value = preferences.getString(Name, Default) ?: return null
        try {
            return MyGson.build().fromJson(value, Type)
        } catch (e: Exception) {
            return null
        }
    }

    fun SET(value: T) {
        val preferences = MyApp.instance.getContext().getSharedPreferences("MyApp", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString(Name, MyGson.build().toJson(value))
        editor.commit()
    }
}