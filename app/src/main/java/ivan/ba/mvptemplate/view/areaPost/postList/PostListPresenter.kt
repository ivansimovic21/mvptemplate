package ivan.ba.mvptemplate.view.areaPost.postList

import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem
import ivan.ba.mvptemplate.util.options.MyOptions

/**
 * Created by isimovic on 30.01.2018..
 */
class PostListPresenter(val model: PostListMVP.Model) : PostListMVP.Presenter {

    val list: ArrayList<VMPostListItem> = arrayListOf()

    lateinit var view: PostListMVP.Activity
    override fun setUpView(view: PostListMVP.Activity) {
        this.view = view
    }

    override fun getPosts() {
        model.getPosts {
            if (it != null) {
                list.clear()
                list.addAll(it)
                view.refreshList()
            } else {
                view.showNoInternetError()
            }
        }
    }

    override fun provideData(): List<VMPostListItem> {
        return list
    }

    override fun provideDataCount(): Int {
        return list.size
    }

    override fun provideDataAtPosition(position: Int): VMPostListItem {
        return list[position]
    }

    override fun addPost(post: DMPost) {
        list.add(VMPostListItem(post, MyOptions.User.Name))
        view.refreshListInsert(list.size - 1)
    }

}