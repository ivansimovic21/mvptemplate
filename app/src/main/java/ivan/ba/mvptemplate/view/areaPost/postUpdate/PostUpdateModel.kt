package ivan.ba.mvptemplate.view.areaPost.postUpdate

import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepPostInterface
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

/**
 * Created by isimovic on 30.01.2018..
 */
class PostUpdateModel(val rep: RepPostInterface) : PostUpdateMVP.Model {

    override fun getPostByID(postID: Long?, id: String?, onFinish: (VMPostListItem?) -> Unit) {
        rep.getPostByID(postID, id, onFinish)
    }

    override fun delete(postID: Long?, id: String?, onFinish: (Boolean) -> Unit) {
        rep.deletePostByID(postID,id, onFinish)
    }

    override fun updatePost(post: DMPost, onFinish: (Boolean) -> Unit) {
        rep.updatePost(post, onFinish)
    }
}