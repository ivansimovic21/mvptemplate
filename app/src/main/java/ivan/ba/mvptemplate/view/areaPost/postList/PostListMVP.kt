package ivan.ba.mvptemplate.view.areaPost.postList

import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

/**
 * Created by isimovic on 30.01.2018..
 */
interface PostListMVP {
    interface Activity {
        fun onClickAddPost()
        fun onListItemClick(postID: Long, id: String)
        fun refreshList()
        fun showNoInternetError()
        fun refreshListInsert(position: Int)
    }

    interface Presenter {
        fun getPosts()
        fun addPost(post: DMPost)
        fun provideData(): List<VMPostListItem>
        fun provideDataCount(): Int
        fun provideDataAtPosition(position: Int): VMPostListItem
        fun setUpView(view: Activity)
    }

    interface Model {
        fun getPosts(onFinish: (List<VMPostListItem>?) -> Unit)
    }
}