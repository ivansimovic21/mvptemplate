package ivan.ba.mvptemplate.view.areaPost.postUpdate

import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem
import ivan.ba.mvptemplate.view.areaPost.bindingModels.PostBindingModel

/**
 * Created by isimovic on 30.01.2018..
 */
class PostUpdatePresenter(val model: PostUpdateMVP.Model) : PostUpdateMVP.Presenter {

    val dataSource: PostBindingModel = PostBindingModel()
    var postID: Long? = null
    var postId: String? = null

    lateinit var view: PostUpdateMVP.Activity

    override fun setUpView(view: PostUpdateMVP.Activity) {
        this.view = view
    }

    override fun getPostByID(postID: Long?, id: String?) {
        this.postID = postID
        this.postId = id
        model.getPostByID(postID, postId, { vmPostListItem: VMPostListItem? ->
            if (vmPostListItem != null) {
                dataSource.text.set(vmPostListItem.text)
                dataSource.title.set(vmPostListItem.title)
                dataSource.userID.set(vmPostListItem.userID)

            } else {
                view.showError()
            }
        })
    }

    override fun provideDataSource(): PostBindingModel {
        return dataSource
    }

    override fun delete() {
        model.delete(postID, postId, { isSuccess: Boolean ->
            view.startPostList()
        })
    }

    override fun updatePost() {
        if (postID == null)
            postID = 0
        if (postId == null)
            postId = ""
        model.updatePost(DMPost(dataSource, postID!!, postId!!), { isSuccess: Boolean ->
            view.startPostList()
        })
    }
}