package ivan.ba.mvptemplate.view.areaPost.postDetails

import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

/**
 * Created by isimovic on 30.01.2018..
 */
class PostDetailsPresenter(val model: PostDetailsMVP.Model) : PostDetailsMVP.Presenter {

    var postID: Long? = null
    var postId: String? = null

    lateinit var view: PostDetailsMVP.Activity
    override fun setUpView(view: PostDetailsMVP.Activity) {
        this.view = view
    }

    override fun getPostByID(postID: Long?, id: String?) {
        this.postID = postID
        this.postId = id
        model.getPostByID(postID, id, { data: VMPostListItem? ->
            if (data != null)
                view.setUpData(data)
            else
                view.showError()
        })
    }

    override fun getPostID(): Long {
        return postID!!
    }

    override fun getId(): String {
        return postId!!
    }

    override fun startUpdate() {
        view.startUpdate(postID, postId)
    }
}