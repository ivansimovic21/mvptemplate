package ivan.ba.mvptemplate.view.areaPost.postAdd

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import ivan.ba.mvptemplate.MyApp
import ivan.ba.mvptemplate.R
import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.databinding.ActivityPostAddBinding
import java.io.Serializable
import javax.inject.Inject


class PostAddActivity : DialogFragment(), PostAddMVP.Activity {


    companion object {
        private val ARG_RunOnPositiveDismiss = "ARG_RunOnPositiveDismiss"
        private val Tag = "Tag"

        fun OpenDialog(activity: AppCompatActivity, runOnPositiveDismiss: (DMPost) -> Unit) {
            val fragmentActivity = PostAddActivity()
            val bundle = Bundle()
            bundle.putSerializable(ARG_RunOnPositiveDismiss, runOnPositiveDismiss as Serializable)
            fragmentActivity.arguments = bundle
            val fragmentManager = activity.supportFragmentManager
            fragmentActivity.show(fragmentManager, Tag)
        }
    }

    @Inject
    lateinit var presenter: PostAddMVP.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        MyApp.graph.inject(this)
        presenter.setUpView(this)
        val binding: ActivityPostAddBinding = DataBindingUtil.inflate(inflater, R.layout.activity_post_add, container, false)
        binding.listeners = Listeners(::onClickAddPost)
        binding.dataSource = presenter.provideDataSource()
        val view = binding.root
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)

        return view
    }

    @UiThread
    override fun onFailedAdd() {
        Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()

    }

    @UiThread
    override fun onAdd(id: Long) {
        val runOnPositiveDismiss = arguments!!.get(ARG_RunOnPositiveDismiss) as (DMPost) -> Unit
        runOnPositiveDismiss(DMPost(presenter.provideDataSource(), id))
        dismiss()
    }

    @UiThread
    override fun onAdd(id: String) {
        val runOnPositiveDismiss = arguments!!.get(ARG_RunOnPositiveDismiss) as (DMPost) -> Unit
        runOnPositiveDismiss(DMPost(presenter.provideDataSource(), id))
        dismiss()
    }

    override fun onClickAddPost() {
        presenter.addPost()
    }

    @UiThread
    override fun showError() {
        Toast.makeText(activity, "Insert all data", Toast.LENGTH_LONG).show()
    }

    override fun onPause() {
        dismiss()
        super.onPause()
    }

    class Listeners(val onClickAddPost: () -> Unit) : View.OnClickListener {
        override fun onClick(v: View?) {
            onClickAddPost()
        }

    }
}
