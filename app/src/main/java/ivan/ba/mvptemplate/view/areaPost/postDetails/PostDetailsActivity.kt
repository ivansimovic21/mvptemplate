package ivan.ba.mvptemplate.view.areaPost.postDetails

import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import ivan.ba.mvptemplate.MyApp
import ivan.ba.mvptemplate.R
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem
import ivan.ba.mvptemplate.databinding.ActivityPostDetailsBinding
import ivan.ba.mvptemplate.util.options.MyOptions
import ivan.ba.mvptemplate.view.areaPost.postUpdate.PostUpdateActivity
import javax.inject.Inject


class PostDetailsActivity : AppCompatActivity(), PostDetailsMVP.Activity {


    companion object {
        val TAG_POSTID = "TAG_POSTID"
    }

    lateinit var binding: ActivityPostDetailsBinding
    @Inject
    lateinit var presenter: PostDetailsMVP.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MyApp.graph.inject(this)
        presenter.setUpView(this)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_details)
        binding.isAdmin = MyOptions.User.GET()!!.roleName == "Admin"
        binding.listeners = Listeners(::onUpdateClick)

        var id: String? = null
        var postID: Long? = null

        if (MyOptions.Firebase.GET()!!)
            id = intent.getStringExtra(TAG_POSTID)
        else
            postID = intent.getLongExtra(TAG_POSTID, 0L)

        presenter.getPostByID(postID, id)
    }

    override fun setUpData(data: VMPostListItem) {
        binding.dataSource = data
    }

    override fun showError() {
        this@PostDetailsActivity.runOnUiThread({
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        })
    }

    override fun startUpdate(postID: Long?, id: String?) {
        val intent = Intent(this, PostUpdateActivity::class.java)
        if (postID != null && postID != 0L)
            intent.putExtra(PostUpdateActivity.ARG_PostID, postID)
        else
            intent.putExtra(PostUpdateActivity.ARG_PostID, id)
        startActivity(intent)
        finish()
    }

    override fun onUpdateClick() {
        presenter.startUpdate()
    }

    class Listeners(val startUpdate: () -> Unit) : View.OnClickListener {
        override fun onClick(p0: View?) {
            startUpdate()
        }
    }
}
