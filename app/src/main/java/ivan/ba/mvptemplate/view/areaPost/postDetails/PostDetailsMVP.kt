package ivan.ba.mvptemplate.view.areaPost.postDetails

import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

/**
 * Created by isimovic on 30.01.2018..
 */
interface PostDetailsMVP {
    interface Activity {
        fun startUpdate(postID: Long?, id: String?)
        fun setUpData(data: VMPostListItem)
        fun showError()
        fun onUpdateClick()
    }

    interface Presenter {
        fun getPostByID(postID: Long?, id: String?)
        fun getPostID(): Long
        fun startUpdate()
        fun setUpView(view: Activity)
        fun getId(): String
    }

    interface Model {
        fun getPostByID(postID: Long?, id: String?, onFinish: (VMPostListItem?) -> Unit)
    }
}