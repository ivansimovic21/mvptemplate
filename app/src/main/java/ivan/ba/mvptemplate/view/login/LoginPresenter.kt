package ivan.ba.mvptemplate.view.login

import ivan.ba.mvptemplate.util.options.MyOptions

/**
 * Created by isimovic on 30.01.2018..
 */

class LoginPresenter(val model: LoginMVP.Model) : LoginMVP.Presenter {

    lateinit var view: LoginMVP.Activity

    val loginDataModel = LoginBindingModel()

    override fun setUpView(view:LoginMVP.Activity){
        this.view = view
    }

    override fun provideBindingDataModel(): LoginBindingModel {
        return loginDataModel
    }

    override fun login() {
        loginDataModel.loginEnabled.set(false)
        model.login(loginDataModel.userName.get()!!, loginDataModel.password.get()!!, { success: Boolean ->
            loginDataModel.userName.set("")
            loginDataModel.password.set("")
            loginDataModel.loginEnabled.set(true)
            if (success)
                view.startPostsActivity()
            else
                view.showFailedLogin()
        })
    }

    override fun setUp() {
        loginDataModel.onlineChecked.set(MyOptions.Online.GET()!!)
        model.setDataForOfflineUse()
    }

    override fun changeIsOnline(isOnline: Boolean) {
        MyOptions.Online.SET(isOnline)
    }

    override fun handleHelpClick() {
        view.openHelpDialog()
    }

}