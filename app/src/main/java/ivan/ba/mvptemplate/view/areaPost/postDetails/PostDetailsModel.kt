package ivan.ba.mvptemplate.view.areaPost.postDetails

import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepPostInterface
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

/**
 * Created by isimovic on 30.01.2018..
 */
class PostDetailsModel(val rep: RepPostInterface) : PostDetailsMVP.Model {

    override fun getPostByID(postID: Long?, id: String?, onFinish: (VMPostListItem?) -> Unit) {
        rep.getPostByID(postID, id, onFinish)
    }
}