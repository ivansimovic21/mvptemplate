package ivan.ba.mvptemplate.view.help

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import ivan.ba.mvptemplate.R
import java.io.Serializable

class HelpDialogActivity : DialogFragment() {

    companion object {
        private val ARG_RunOnPositiveDismiss = "ARG_RunOnPositiveDismiss"
        private val Tag = "Tag"

        fun OpenDialog(activity: AppCompatActivity, runOnPositiveDismiss: (Boolean) -> Unit) {
            val fragmentActivity = HelpDialogActivity()
            val bundle = Bundle()
            bundle.putSerializable(ARG_RunOnPositiveDismiss, runOnPositiveDismiss as Serializable)
            fragmentActivity.arguments = bundle
            val fragmentManager = activity.supportFragmentManager
            fragmentActivity.show(fragmentManager, Tag)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = layoutInflater.inflate(R.layout.activity_help_dialog,container,false)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return view
    }
}
