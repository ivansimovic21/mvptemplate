package ivan.ba.mvptemplate.view.login

import ivan.ba.mvptemplate.data.datamodels.DMLoggedUser

/**
 * Created by isimovic on 30.01.2018..
 */
interface LoginMVP {
    interface Activity {
        fun startPostsActivity()
        fun onClickLogin()
        fun onCheckedChangedIsOnline(isChecked:Boolean)
        fun showFailedLogin()
        fun onClickHelp()
        fun openHelpDialog()
    }

    interface Presenter {
        fun login()
        fun changeIsOnline(isOnline: Boolean)
        fun setUp()
        fun provideBindingDataModel(): LoginBindingModel
        fun handleHelpClick()
        fun setUpView(view: Activity)
    }

    interface Model {
        fun login(userName:String,password:String, onFinish: (Boolean) -> Unit)
        fun setDataForOfflineUse()
    }
}