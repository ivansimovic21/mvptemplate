package ivan.ba.mvptemplate.view.areaPost.postAdd

import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepPostInterface

/**
 * Created by isimovic on 30.01.2018..
 */
class PostAddModel(val rep: RepPostInterface) : PostAddMVP.Model {

    override fun addPost(post: DMPost, onFinish: (Long?, String?) -> Unit) {
        rep.insertPost(post, onFinish)
    }

}