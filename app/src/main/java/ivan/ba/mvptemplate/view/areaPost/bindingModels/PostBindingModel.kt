package ivan.ba.mvptemplate.view.areaPost.bindingModels

import android.databinding.ObservableField

/**
 * Created by isimovic on 30.01.2018..
 */
data class PostBindingModel(
        var title: ObservableField<String> = ObservableField(""),
        var text: ObservableField<String> = ObservableField(""),
        var userID: ObservableField<Long> = ObservableField(0)
){
    constructor(title: String, text: String, userID: Long) : this() {
        this.title = ObservableField(title)
        this.text = ObservableField(text)
        this.userID = ObservableField(userID)
    }
}