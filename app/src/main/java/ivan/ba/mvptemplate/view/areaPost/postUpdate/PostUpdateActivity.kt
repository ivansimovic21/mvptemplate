package ivan.ba.mvptemplate.view.areaPost.postUpdate

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import ivan.ba.mvptemplate.MyApp
import ivan.ba.mvptemplate.R
import ivan.ba.mvptemplate.databinding.ActivityPostUpdateBinding
import ivan.ba.mvptemplate.util.options.MyOptions
import ivan.ba.mvptemplate.view.areaPost.postDetails.PostDetailsActivity
import ivan.ba.mvptemplate.view.areaPost.postList.PostListActivity
import javax.inject.Inject

class PostUpdateActivity : AppCompatActivity(), PostUpdateMVP.Activity {

    companion object {
        val ARG_PostID = "ARG_PostID"
    }

    lateinit var binding: ActivityPostUpdateBinding
    @Inject
    lateinit var presenter: PostUpdateMVP.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MyApp.graph.inject(this)
        presenter.setUpView(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_update)
        binding.listeners = Listeners(::onClickUpdate, ::onClickDelete)
        binding.dataSource = presenter.provideDataSource()
        var id: String? = null
        var postID: Long? = null

        if (MyOptions.Firebase.GET()!!)
            id = intent.getStringExtra(PostUpdateActivity.ARG_PostID)
        else
            postID = intent.getLongExtra(PostUpdateActivity.ARG_PostID, 0L)

        presenter.getPostByID(postID, id)
    }

    override fun onClickUpdate() {
        presenter.updatePost()
    }

    override fun onClickDelete() {
        presenter.delete()
    }

    override fun startPostList() {
        val intent = Intent(this, PostListActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun showError() {
        PostUpdateActivity@ this.runOnUiThread(java.lang.Runnable {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        })
    }

    class Listeners(val onSave: () -> Unit, val onDelete: () -> Unit) : View.OnClickListener {
        override fun onClick(v: View?) {
            if (v != null) {
                if (v.id == R.id.PostUpdate_btnSave)
                    onSave()
                else
                    onDelete()
            }
        }
    }
}
