package ivan.ba.mvptemplate.view.areaPost.postList

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import ivan.ba.mvptemplate.MyApp
import ivan.ba.mvptemplate.R
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem
import ivan.ba.mvptemplate.databinding.ActivityPostListBinding
import ivan.ba.mvptemplate.databinding.ListItemPostBinding
import ivan.ba.mvptemplate.util.options.MyOptions
import ivan.ba.mvptemplate.view.areaPost.postAdd.PostAddActivity
import ivan.ba.mvptemplate.view.areaPost.postDetails.PostDetailsActivity
import kotlinx.android.synthetic.main.activity_post_list.*
import javax.inject.Inject


class PostListActivity : AppCompatActivity(), PostListMVP.Activity {

    private lateinit var listAdapter: ListAdapter
    lateinit var binding: ActivityPostListBinding

    @Inject
    lateinit var presenter: PostListMVP.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MyApp.graph.inject(this)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_list)
        binding.isUser = MyOptions.User.GET()!!.roleName == "User"
        binding.listeners = Listeners(::onClickAddPost)

        presenter.setUpView(this)
        presenter.getPosts()

        listAdapter = ListAdapter(this, presenter, ::onListItemClick)
        Posts_lvPosts.layoutManager = LinearLayoutManager(this)
        Posts_lvPosts.adapter = listAdapter
    }

    override fun showNoInternetError() {
        this@PostListActivity.runOnUiThread({
            Toast.makeText(this, "No internet", Toast.LENGTH_LONG).show()
        })
    }

    override fun refreshList() {
        this@PostListActivity.runOnUiThread({
            listAdapter.notifyDataSetChanged()
        })
    }

    override fun onClickAddPost() {
        PostAddActivity.OpenDialog(this, presenter::addPost)
    }

    override fun refreshListInsert(position: Int) {
        this@PostListActivity.runOnUiThread({
            listAdapter.notifyItemInserted(position)
        })
    }

    override fun onListItemClick(postID: Long, id: String) {
        val intent = Intent(this, PostDetailsActivity::class.java)
        if ( postID != 0L)
            intent.putExtra(PostDetailsActivity.TAG_POSTID, postID)
        else
            intent.putExtra(PostDetailsActivity.TAG_POSTID, id)
        startActivity(intent)
    }

    class Listeners(val onClickAddPost: () -> Unit) : View.OnClickListener {
        override fun onClick(v: View?) {
            onClickAddPost()
        }
    }

    class ListAdapter(context: Context, val presenter: PostListMVP.Presenter, onItemClick: (Long, String) -> Unit) : RecyclerView.Adapter<ViewHolder>() {
        val onItemClick: (Long, String) -> Unit
        private val mInflator: LayoutInflater

        init {
            this.mInflator = LayoutInflater.from(context)
            this.onItemClick = onItemClick
        }

        override fun getItemCount(): Int {
            return presenter.provideDataCount()
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item: VMPostListItem = presenter.provideDataAtPosition(position)
            val binding: ListItemPostBinding = DataBindingUtil.getBinding(holder.itemView)!!
            binding.dataSource = item
            binding.listeners = Listeners(onItemClick, item.postID, item.id)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding: ListItemPostBinding = ListItemPostBinding.inflate(mInflator, parent, false)
            return ViewHolder(binding.root)
        }

        override fun getItemId(position: Int): Long {
            return presenter.provideDataAtPosition(position).postID
        }

        class Listeners(var onItemClick: (Long, String) -> Unit, var postID: Long, var id: String) : View.OnClickListener {
            override fun onClick(v: View?) {
                onItemClick(postID, id)
            }
        }
    }

    class ViewHolder(row: View?) : RecyclerView.ViewHolder(row) {
        init {
        }
    }
}
