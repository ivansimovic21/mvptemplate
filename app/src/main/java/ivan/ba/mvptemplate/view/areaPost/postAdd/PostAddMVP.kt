package ivan.ba.mvptemplate.view.areaPost.postAdd

import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.view.areaPost.bindingModels.PostBindingModel

/**
 * Created by isimovic on 30.01.2018..
 */
interface PostAddMVP {
    interface Activity {
        fun onClickAddPost()
        fun onFailedAdd()
        fun onAdd(id: Long)
        fun onAdd(id: String)
        fun showError()
    }

    interface Presenter {
        fun addPost()
        fun provideDataSource(): PostBindingModel
        fun validateInput(): Boolean
        fun setUpView(view: Activity)
    }

    interface Model {
        fun addPost(post:DMPost,onFinish:(Long?, String?) -> Unit)
    }
}