package ivan.ba.mvptemplate.view.areaPost.postAdd

import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.view.areaPost.bindingModels.PostBindingModel

/**
 * Created by isimovic on 30.01.2018..
 */
class PostAddPresenter(val model: PostAddMVP.Model) : PostAddMVP.Presenter {
    val dataSource = PostBindingModel()

    lateinit var view: PostAddMVP.Activity

    override fun setUpView(view: PostAddMVP.Activity) {
        this.view = view
    }

    override fun addPost() {
        if (validateInput()) {
            model.addPost(DMPost(dataSource), { l: Long?, s: String? ->
                if (l != null) {
                    if (l != 0L) {
                        view.onAdd(l)
                    } else {
                        view.onFailedAdd()
                    }
                } else if (s != null) {
                    if (s != "")
                        view.onAdd(s)
                    else
                        view.onFailedAdd()
                } else
                    view.onFailedAdd()
            })
        } else {
            view.showError()
        }
    }

    override fun validateInput(): Boolean {
        var valid = true
        if (dataSource.text.get().equals(""))
            valid = false
        else if (dataSource.title.get().equals(""))
            valid = false
        return valid
    }

    override fun provideDataSource(): PostBindingModel {
        return dataSource
    }
}