package ivan.ba.mvptemplate.view.areaPost.postUpdate

import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem
import ivan.ba.mvptemplate.view.areaPost.bindingModels.PostBindingModel

/**
 * Created by isimovic on 30.01.2018..
 */
interface PostUpdateMVP {

    interface Activity {
        fun onClickUpdate()
        fun showError()
        fun onClickDelete()
        fun startPostList()
    }

    interface Presenter {
        fun getPostByID(postID: Long?, id: String?)
        fun provideDataSource(): PostBindingModel
        fun delete()
        fun updatePost()
        fun setUpView(view: Activity)
    }

    interface Model {
        fun getPostByID(postID: Long?, id: String?, onFinish: (VMPostListItem?) -> Unit)
        fun delete(postID: Long?, id: String?, onFinish: (Boolean) -> Unit)
        fun updatePost(post: DMPost, onFinish: (Boolean) -> Unit)
    }
}