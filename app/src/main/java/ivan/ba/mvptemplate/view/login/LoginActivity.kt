package ivan.ba.mvptemplate.view.login

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.CompoundButton
import android.widget.Toast
import ivan.ba.mvptemplate.MyApp
import ivan.ba.mvptemplate.R
import ivan.ba.mvptemplate.databinding.ActivityLoginBinding
import ivan.ba.mvptemplate.view.areaPost.postList.PostListActivity
import ivan.ba.mvptemplate.view.help.HelpDialogActivity
import javax.inject.Inject


class LoginActivity : AppCompatActivity(), LoginMVP.Activity {

    @Inject lateinit var presenter: LoginMVP.Presenter
    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MyApp.graph.inject(this)
        presenter.setUpView(this)
        presenter.setUp()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.listeners = Listeners(::onClickHelp, ::onClickLogin, ::onCheckedChangedIsOnline)
        binding.dataSource = presenter.provideBindingDataModel()
    }

    override fun startPostsActivity() {
        this@LoginActivity.runOnUiThread({
            val intent = Intent(this, PostListActivity::class.java)
            startActivity(intent)
        })
    }

    override fun showFailedLogin() {
        this@LoginActivity.runOnUiThread({
            Toast.makeText(this, "Login failed", Toast.LENGTH_LONG).show()
        })
    }

    override fun onClickLogin() {
        presenter.login()
    }

    override fun onClickHelp() {
        presenter.handleHelpClick()
    }

    override fun openHelpDialog() {
        this@LoginActivity.runOnUiThread({
            HelpDialogActivity.OpenDialog(this, {

            })
        })
    }

    override fun onCheckedChangedIsOnline(isChecked: Boolean) {
        presenter.changeIsOnline(isChecked)
    }

    class Listeners(val onClickHelp: () -> Unit, val onClickLogin: () -> Unit, val onCheckedChangedIsOnline: (Boolean) -> Unit) : View.OnClickListener, CompoundButton.OnCheckedChangeListener {

        override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
            onCheckedChangedIsOnline(isChecked)
        }

        override fun onClick(v: View?) {
            if (v!!.id.equals(R.id.Login_btnLogin))
                onClickLogin()
            else if (v.id.equals(R.id.Login_btnDialogHelp))
                onClickHelp()
        }

    }
}
