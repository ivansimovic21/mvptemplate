package ivan.ba.mvptemplate.view.login

import dagger.Module
import dagger.Provides
import ivan.ba.mvptemplate.data.reposetory.RepUser
import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepUserInterface
import javax.inject.Singleton

@Module
class LoginModule {
    @Provides
    fun provideLoginPresenter(model: LoginMVP.Model): LoginMVP.Presenter {
        return LoginPresenter(model)
    }

    @Provides
    @Singleton
    fun provideLoginModel(reposetory: RepUserInterface): LoginMVP.Model {
        return LoginModel(reposetory)
    }

    @Provides
    @Singleton
    fun provideUserReposetory(): RepUserInterface {
        return RepUser()
    }
}