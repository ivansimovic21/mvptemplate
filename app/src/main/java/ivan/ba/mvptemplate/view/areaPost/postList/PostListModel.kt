package ivan.ba.mvptemplate.view.areaPost.postList

import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepPostInterface
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

/**
 * Created by isimovic on 30.01.2018..
 */
class PostListModel(val rep: RepPostInterface) : PostListMVP.Model {

    override fun getPosts(onFinish: (List<VMPostListItem>?) -> Unit) {
        rep.getPosts(onFinish)
    }
}