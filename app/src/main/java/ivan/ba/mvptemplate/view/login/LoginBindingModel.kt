package ivan.ba.mvptemplate.view.login

import android.databinding.ObservableField
import ivan.ba.mvptemplate.util.options.MyOptions

/**
 * Created by isimovic on 30.01.2018..
 */
data class LoginBindingModel(
        var userName: ObservableField<String> = ObservableField("user"),
        var password: ObservableField<String> = ObservableField("user"),
        var loginEnabled: ObservableField<Boolean> = ObservableField(true),
        var onlineChecked: ObservableField<Boolean> = ObservableField(true)
) {}