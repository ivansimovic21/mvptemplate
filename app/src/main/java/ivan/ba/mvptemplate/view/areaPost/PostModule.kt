package ivan.ba.mvptemplate.view.areaPost

import dagger.Module
import dagger.Provides
import ivan.ba.mvptemplate.data.reposetory.RepPost
import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepPostInterface
import ivan.ba.mvptemplate.view.areaPost.postAdd.PostAddMVP
import ivan.ba.mvptemplate.view.areaPost.postAdd.PostAddModel
import ivan.ba.mvptemplate.view.areaPost.postAdd.PostAddPresenter
import ivan.ba.mvptemplate.view.areaPost.postDetails.PostDetailsMVP
import ivan.ba.mvptemplate.view.areaPost.postDetails.PostDetailsModel
import ivan.ba.mvptemplate.view.areaPost.postDetails.PostDetailsPresenter
import ivan.ba.mvptemplate.view.areaPost.postList.PostListMVP
import ivan.ba.mvptemplate.view.areaPost.postList.PostListModel
import ivan.ba.mvptemplate.view.areaPost.postList.PostListPresenter
import ivan.ba.mvptemplate.view.areaPost.postUpdate.PostUpdateMVP
import ivan.ba.mvptemplate.view.areaPost.postUpdate.PostUpdateModel
import ivan.ba.mvptemplate.view.areaPost.postUpdate.PostUpdatePresenter
import javax.inject.Singleton

@Module
class PostModule {
    @Provides
    @Singleton
    fun provideRepPost(): RepPostInterface {
        return RepPost()
    }

    @Provides
    fun providePostAddPresenter(model: PostAddMVP.Model): PostAddMVP.Presenter {
        return PostAddPresenter(model)
    }

    @Provides
    @Singleton
    fun providePostAddModel(repository: RepPostInterface): PostAddMVP.Model {
        return PostAddModel(repository)
    }

    @Provides
    fun providePostDetailsPresenter(model: PostDetailsMVP.Model): PostDetailsMVP.Presenter {
        return PostDetailsPresenter(model)
    }

    @Provides
    @Singleton
    fun providePostDetailsModel(repository: RepPostInterface): PostDetailsMVP.Model {
        return PostDetailsModel(repository)
    }

    @Provides
    fun providePostListPresenter(model: PostListMVP.Model): PostListMVP.Presenter {
        return PostListPresenter(model)
    }

    @Provides
    @Singleton
    fun providePostListModel(repository: RepPostInterface): PostListMVP.Model {
        return PostListModel(repository)
    }

    @Provides
    fun providePostUpdatePresenter(model: PostUpdateMVP.Model): PostUpdateMVP.Presenter {
        return PostUpdatePresenter(model)
    }

    @Provides
    @Singleton
    fun providePostUpdateModel(repository: RepPostInterface): PostUpdateMVP.Model {
        return PostUpdateModel(repository)
    }

}