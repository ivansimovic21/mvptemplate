package ivan.ba.mvptemplate.view.login

import ivan.ba.mvptemplate.data.datamodels.DMRole
import ivan.ba.mvptemplate.data.datamodels.DMUser
import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepUserInterface

/**
 * Created by isimovic on 30.01.2018..
 */
class LoginModel(val repUser: RepUserInterface) : LoginMVP.Model {

    override fun login(userName: String, password: String, onFinish: (Boolean) -> Unit) {
        repUser.login(userName, password, onFinish)
    }

    override fun setDataForOfflineUse() {
        repUser.select {
            if (it.size == 0) {
                repUser.insertRole(DMRole("",1, "Admin"))
                repUser.insertRole(DMRole("",2, "User"))
                repUser.insertRole(DMRole("",3, "Reader"))
                repUser.insertUser(DMUser("",1, "",1,"Admin", "Admin", "admin", "admin"))
                repUser.insertUser(DMUser("",2, "",2,"User", "User", "user", "user"))
                repUser.insertUser(DMUser("",3, "",3,"Reader", "Reader", "reader", "reader"))
            }
        }
    }

}