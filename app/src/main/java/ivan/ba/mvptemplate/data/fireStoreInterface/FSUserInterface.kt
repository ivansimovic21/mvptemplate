package ivan.ba.mvptemplate.data.fireStoreInterface

import ivan.ba.mvptemplate.data.datamodels.DMUser

interface FSUserInterface{

    fun login(userName: String, password: String, onFinish: (Boolean) -> Unit)
    fun select(onSuccess: (List<DMUser>) -> Unit)
}