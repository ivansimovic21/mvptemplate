package ivan.ba.mvptemplate.data.SQLite.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import ivan.ba.mvptemplate.data.datamodels.DMUser

/**
 * Created by isimovic on 05.01.2018..
 */
@Entity(tableName = "USERS",
        foreignKeys = arrayOf(ForeignKey(entity = EntityRole::class,
                parentColumns = arrayOf("roleID"),
                childColumns = arrayOf("roleID"),
                onDelete = ForeignKey.CASCADE)))
data class EntityUser(
        @PrimaryKey(autoGenerate = true)
        val userID: Long,
        @ColumnInfo(index = true)
        val roleID: Long,
        val name: String,
        val userName: String,
        val password: String) {
    constructor(item: DMUser) : this(item.userID, item.roleID, item.name, item.userName, item.password)
}