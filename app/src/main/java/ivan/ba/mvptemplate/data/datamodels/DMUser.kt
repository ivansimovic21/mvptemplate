package ivan.ba.mvptemplate.data.datamodels

/**
 * Created by isimovic on 08.01.2018..
 */
data class DMUser(
        var id: String,
        var userID: Long,
        var roleId: String,
        var roleID: Long,
        var roleName: String,
        var name: String,
        var userName: String,
        var password: String)
