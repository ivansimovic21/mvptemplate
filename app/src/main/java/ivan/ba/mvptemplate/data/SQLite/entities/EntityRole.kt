package ivan.ba.mvptemplate.data.SQLite.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import ivan.ba.mvptemplate.data.datamodels.DMRole

/**
 * Created by isimovic on 05.01.2018..
 */
@Entity(tableName = "ROLES")
data class EntityRole(
        @PrimaryKey(autoGenerate = true)
        val roleID: Long,
        val name: String) {
    constructor(item: DMRole) : this(item.roleID, item.name)
}