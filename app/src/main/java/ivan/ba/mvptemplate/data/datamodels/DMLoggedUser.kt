package ivan.ba.mvptemplate.data.datamodels

/**
 * Created by isimovic on 09.01.2018..
 */
data class DMLoggedUser(
        var id: String,
        var userID: Long,
        var roleId: String,
        var roleID: Long,
        var roleName: String,
        var userName: String)
