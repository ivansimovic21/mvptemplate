package ivan.ba.mvptemplate.data.SQLite.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import ivan.ba.mvptemplate.data.datamodels.DMPost

/**
 * Created by isimovic on 05.01.2018..
 */
@Entity(tableName = "POSTS",
        foreignKeys = arrayOf(ForeignKey(entity = EntityUser::class,
                parentColumns = arrayOf("userID"),
                childColumns = arrayOf("userID"),
                onDelete = ForeignKey.CASCADE)))

data class EntityPost(
        @PrimaryKey(autoGenerate = true)
        val postID: Long,
        @ColumnInfo(index = true)
        val userID: Long,
        val title: String,
        val text: String) {
    constructor(item: DMPost) : this(item.postID, item.userID, item.title, item.text)
}