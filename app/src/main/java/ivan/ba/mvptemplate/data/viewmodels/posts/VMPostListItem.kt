package ivan.ba.mvptemplate.data.viewmodels.posts

import ivan.ba.mvptemplate.data.datamodels.DMPost


/**
 * Created by isimovic on 05.01.2018..
 */
data class VMPostListItem (var id:String,var postID:Long,var userId:String, var userID:Long, var userName:String, var title:String, var text:String){
    constructor(item: DMPost, username:String) : this("",item.postID,"", item.userID,username, item.title, item.text)
}