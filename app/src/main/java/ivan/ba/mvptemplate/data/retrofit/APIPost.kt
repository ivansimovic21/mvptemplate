package ivan.ba.mvptemplate.data.retrofit

import io.reactivex.Flowable
import io.reactivex.Single
import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by isimovic on 08.01.2018..
 */
interface APIPost{
    @GET("api/Post/GetAll")
    fun getPosts(): Call<List<VMPostListItem>>

    @GET("api/Post/GetById/{id}")
    fun getPostByID(@Path("id") id:Long): Call<VMPostListItem>


    @POST("api/Post/Insert")
    fun insertPost(@Body post: DMPost): Call<Long>

    @PUT("api/Post/Update")
    fun updatePost(@Body post: DMPost): Call<DMPost>

    @DELETE("api/Post/Delete/{id}")
    fun deletePostByID(@Path("id") id:Long): Call<Boolean>
}