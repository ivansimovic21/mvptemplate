package ivan.ba.mvptemplate.data.datamodels

import ivan.ba.mvptemplate.view.areaPost.bindingModels.PostBindingModel

/**
 * Created by isimovic on 08.01.2018..
 */
data class DMPost(
        var id: String,
        var postID: Long,
        var userId: String,
        var userID: Long,
        var title: String,
        var text: String) {
    constructor(item: PostBindingModel) : this("", 0, "", item.userID.get()!!, item.title.get()!!, item.text.get()!!)
    constructor(item: PostBindingModel, postID: Long) : this("", postID, "", item.userID.get()!!, item.title.get()!!, item.text.get()!!)
    constructor(item: PostBindingModel, postID: String) : this(postID, 0, "", 0, item.title.get()!!, item.text.get()!!)
    constructor(item: PostBindingModel, postID: Long, id: String) : this(id, postID, "", 0, item.title.get()!!, item.text.get()!!)
}