package ivan.ba.mvptemplate.data.reposetoryInterfaces

import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

/**
 * Created by isimovic on 30.01.2018..
 */
interface RepPostInterface {

    fun getPosts(onFinish: (List<VMPostListItem>?) -> Unit)

    fun getPostByID(postID: Long?, id: String?, onFinish: (VMPostListItem?) -> Unit)

    fun insertPost(post: DMPost, onFinish: (Long?, String?) -> Unit)

    fun updatePost(post: DMPost, onFinish: (Boolean) -> Unit)

    fun deletePostByID(postID: Long?, id: String?, onFinish: (Boolean) -> Unit)
}