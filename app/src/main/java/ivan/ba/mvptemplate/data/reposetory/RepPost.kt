package ivan.ba.mvptemplate.data.reposetory

import android.os.AsyncTask
import ivan.ba.mvptemplate.MyApp
import ivan.ba.mvptemplate.data.SQLite.dao.DAOPost
import ivan.ba.mvptemplate.data.SQLite.entities.EntityPost
import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.fireStore.FSPost
import ivan.ba.mvptemplate.data.fireStore.FSUser
import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepPostInterface
import ivan.ba.mvptemplate.data.retrofit.APIPost
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem
import ivan.ba.mvptemplate.util.options.MyOptions
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Created by isimovic on 05.01.2018..
 */
class RepPost : RepPostInterface {
    @Inject
    lateinit var daoPost: DAOPost
    @Inject
    lateinit var retrofit: Retrofit

    val fsPost = FSPost()

    init {
        MyApp.graphRepository.inject(this)
    }

    override fun getPosts(onFinish: (List<VMPostListItem>?) -> Unit) {
        AsyncTask.execute {
            try {
                if (MyOptions.Firebase.GET()!!) {
                    fsPost.getPosts(onFinish)
                } else
                    if (MyOptions.Online.GET()!!) {
                        val create = retrofit.create(APIPost::class.java)
                        val getPosts = create.getPosts()
                        val response = getPosts.execute()
                        if (response.isSuccessful) {
                            onFinish(response.body()!!)
                        } else {
                            onFinish(null)
                        }
                    } else {
                        onFinish(daoPost.getPosts())
                    }
            } catch (e: Exception) {
                onFinish(null)
            }
        }
    }

    override fun getPostByID(postID: Long?, id: String?, onFinish: (VMPostListItem?) -> Unit) {
        AsyncTask.execute {
            try {
                if (MyOptions.Firebase.GET()!!) {
                    fsPost.getPostByID(id!!, onFinish)
                } else
                    if (MyOptions.Online.GET()!!) {
                        val create = retrofit.create(APIPost::class.java)
                        val getPostByID = create.getPostByID(postID!!)
                        val response = getPostByID.execute()
                        if (response.isSuccessful) {
                            onFinish(response.body()!!)
                        } else {
                            onFinish(null)
                        }
                    } else {
                        onFinish(daoPost.getPostByID(postID!!))
                    }
            } catch (e: Exception) {
                onFinish(null)
            }
        }
    }

    override fun insertPost(post: DMPost, onFinish: (Long?, String?) -> Unit) {
        AsyncTask.execute {
            try {
                if (MyOptions.Firebase.GET()!!) {
                    fsPost.insertPost(post.userId, "", post.title, post.text, onFinish)
                } else
                    if (MyOptions.Online.GET()!!) {
                        val client: APIPost = retrofit.create(APIPost::class.java)
                        val insertPost: Call<Long> = client.insertPost(post)
                        val response = insertPost.execute()

                        if (response.isSuccessful) {
                            onFinish(response.body()!!, null)
                        } else {
                            onFinish(0, null)
                        }
                    } else {
                        post.userID = MyOptions.User.GET()!!.userID // Ovo netreba za web jer to WEB API sam vuče uz pomoć auth ključa
                        onFinish(daoPost.insertPost(EntityPost(post)), null)
                    }
            } catch (e: Exception) {
                onFinish(0, null)
            }
        }
    }

    override fun updatePost(post: DMPost, onFinish: (Boolean) -> Unit) {
        AsyncTask.execute {
            try {
                if (MyOptions.Firebase.GET()!!) {
                    fsPost.updatePost(post.id, post.userId, "", post.title, post.text, onFinish)
                } else
                    if (MyOptions.Online.GET()!!) {
                        val client = retrofit.create(APIPost::class.java)
                        val insertPost: Call<DMPost> = client.updatePost(post)
                        val response = insertPost.execute()

                        if (response.isSuccessful) {
                            onFinish(response.body() != null)
                        } else {
                            onFinish(false)
                        }
                    } else {
                        daoPost.updatePost(EntityPost(post))
                        onFinish(true)
                    }

            } catch (e: Exception) {
                onFinish(false)
            }
        }
    }

    override fun deletePostByID(postID: Long?, id: String?, onFinish: (Boolean) -> Unit) {
        AsyncTask.execute {
            try {
                if (MyOptions.Firebase.GET()!!) {
                    fsPost.deletePostByID(id!!, onFinish)
                } else
                    if (MyOptions.Online.GET()!!) {
                        val client = retrofit.create(APIPost::class.java)
                        val deletePostByID: Call<Boolean> = client.deletePostByID(postID!!)
                        val response = deletePostByID.execute()
                        if (response.isSuccessful) {
                            onFinish(response.body()!!)
                        } else {
                            onFinish(false)
                        }
                    } else {
                        daoPost.deletePostByID(postID!!)
                        onFinish(true)
                    }
            } catch (e: Exception) {
                onFinish(false)
            }
        }
    }
}