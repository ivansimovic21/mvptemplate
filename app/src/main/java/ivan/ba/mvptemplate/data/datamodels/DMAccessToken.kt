package ivan.ba.mvptemplate.data.datamodels

/**
 * Created by isimovic on 08.01.2018..
 */
data class DMAccessToken(
        var access_token: String,
        var token_type: String,
        var UserID: Long,
        var RoleID: Long,
        var RoleName: String,
        var UserName: String,
        var refresh_token: String,
        var expires_in: Int)