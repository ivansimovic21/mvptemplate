package ivan.ba.mvptemplate.data.datamodels

/**
 * Created by isimovic on 08.01.2018..
 */
data class DMRole(
        var id: String,
        var roleID: Long,
        var name: String)