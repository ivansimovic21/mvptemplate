package ivan.ba.mvptemplate.data.SQLite

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import ivan.ba.mvptemplate.data.SQLite.dao.DAOPost
import ivan.ba.mvptemplate.data.SQLite.dao.DAOUser
import ivan.ba.mvptemplate.data.SQLite.entities.EntityPost
import ivan.ba.mvptemplate.data.SQLite.entities.EntityRole
import ivan.ba.mvptemplate.data.SQLite.entities.EntityUser

/**
 * Created by isimovic on 05.01.2018..
 */
@Database(entities = arrayOf(EntityPost::class, EntityUser::class, EntityRole::class), version = 1)
abstract class MyDatabase : RoomDatabase() {

    abstract fun userDao(): DAOUser
    abstract fun postDao(): DAOPost

    companion object {

        @Volatile private var INSTANCE: MyDatabase? = null

        fun getInstance(context: Context): MyDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        MyDatabase::class.java, "MyDatabase.db")
                        .build()
    }
}
