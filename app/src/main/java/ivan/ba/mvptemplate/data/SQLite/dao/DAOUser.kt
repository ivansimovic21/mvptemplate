package ivan.ba.mvptemplate.data.SQLite.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import ivan.ba.mvptemplate.data.SQLite.entities.EntityRole
import ivan.ba.mvptemplate.data.SQLite.entities.EntityUser
import ivan.ba.mvptemplate.data.datamodels.DMLoggedUser
import ivan.ba.mvptemplate.data.datamodels.DMUser

/**
 * Created by isimovic on 05.01.2018..
 */
@Dao
interface DAOUser {
    @Query("SELECT USERS.userID,USERS.roleID,ROLES.name as roleName, USERS.userName, '' as id,'' as roleId FROM USERS JOIN ROLES ON USERS.roleID = ROLES.roleID WHERE Username = :userName AND password = :password")
    fun login(userName: String,password:String): DMLoggedUser?

    @Query("SELECT USERS.*,ROLES.name as roleName,'' as id,'' as roleId FROM USERS JOIN ROLES ON USERS.roleID = ROLES.roleID")
    fun select(): List<DMUser>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: EntityUser)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRole(role: EntityRole)
}
