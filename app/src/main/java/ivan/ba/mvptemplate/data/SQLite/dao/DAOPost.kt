package ivan.ba.mvptemplate.data.SQLite.dao

import android.arch.persistence.room.*
import ivan.ba.mvptemplate.data.SQLite.entities.EntityPost
import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

/**
 * Created by isimovic on 05.01.2018..
 */
@Dao
interface DAOPost {
    @Query("SELECT POSTS.postID, POSTS.userID as userID,USERS.Username ,POSTS.title,POSTS.text,'' as id,'' as userId FROM POSTS JOIN USERS ON POSTS.userID = USERS.userID")
    fun getPosts(): List<VMPostListItem>

    @Query("SELECT POSTS.postID, POSTS.userID as userID,USERS.Username ,POSTS.title,POSTS.text,'' as id,'' as userId FROM POSTS JOIN USERS ON POSTS.userID = USERS.userID WHERE POSTS.postID = :postID")
    fun getPostByID(postID : Long): VMPostListItem


    @Query("SELECT *,'' as id,'' as userId FROM POSTS")
    fun select(): List<DMPost>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPost(post: EntityPost) : Long

    @Delete
    fun deletePost(post: EntityPost)

    @Query("DELETE FROM POSTS WHERE postID = :postID")
    fun deletePostByID(postID: Long)

    @Update
    fun updatePost(post: EntityPost)
}
