package ivan.ba.mvptemplate.data.fireStoreInterface

import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

interface FSPostInterface{

    fun getPosts(onFinish: (List<VMPostListItem>?) -> Unit)
    fun insertPost(userID: String, userName: String, title: String, text: String, onFinish: (Long?,String?) -> Unit)
    fun updatePost(id: String, userID: String, userName: String, title: String, text: String, onFinish: (Boolean) -> Unit)
    fun deletePostByID(id: String, onFinish: (Boolean) -> Unit)
    fun getPostByID(postID: String, onFinish: (VMPostListItem?) -> Unit)
}