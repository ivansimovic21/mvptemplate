package ivan.ba.mvptemplate.data.reposetoryInterfaces

import ivan.ba.mvptemplate.data.datamodels.DMLoggedUser
import ivan.ba.mvptemplate.data.datamodels.DMRole
import ivan.ba.mvptemplate.data.datamodels.DMUser

/**
 * Created by isimovic on 30.01.2018..
 */
interface RepUserInterface{
    fun login(userName: String, password: String, onFinish: (Boolean) -> Unit)
    fun select(onSuccess: (List<DMUser>) -> Unit)
    fun insertUser(user: DMUser)
    fun insertRole(role: DMRole)
}