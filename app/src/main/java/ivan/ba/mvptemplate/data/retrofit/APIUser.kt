package ivan.ba.mvptemplate.data.retrofit

import ivan.ba.mvptemplate.data.datamodels.DMAccessToken
import ivan.ba.mvptemplate.data.datamodels.DMLoggedUser
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by isimovic on 08.01.2018..
 */
interface APIUser{
    @GET("api/User/GetUser")
    fun GetUser(): Call<DMLoggedUser>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("/token")
    fun Token(@Field("grant_type")grantType: String,@Field("username")username: String,@Field("password")password: String): Call<DMAccessToken>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("/token")
    fun RefreshToken(@Field("grant_type")grantType: String,@Field("refresh_token")refreshToken: String): Call<DMAccessToken>
}