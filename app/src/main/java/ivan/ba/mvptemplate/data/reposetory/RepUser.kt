package ivan.ba.mvptemplate.data.reposetory

import android.os.AsyncTask
import ivan.ba.mvptemplate.MyApp
import ivan.ba.mvptemplate.data.SQLite.dao.DAOUser
import ivan.ba.mvptemplate.data.SQLite.entities.EntityRole
import ivan.ba.mvptemplate.data.SQLite.entities.EntityUser
import ivan.ba.mvptemplate.data.datamodels.DMAccessToken
import ivan.ba.mvptemplate.data.datamodels.DMLoggedUser
import ivan.ba.mvptemplate.data.datamodels.DMRole
import ivan.ba.mvptemplate.data.datamodels.DMUser
import ivan.ba.mvptemplate.data.fireStore.FSUser
import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepUserInterface
import ivan.ba.mvptemplate.data.retrofit.APIUser
import ivan.ba.mvptemplate.util.options.MyOptions
import ivan.ba.mvptemplate.util.retrofit.TokenAuthenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Named


/**
 * Created by isimovic on 05.01.2018..
 */
open class RepUser : RepUserInterface {
    @Inject
    lateinit var dao: DAOUser
    @Inject
    lateinit var retrofit: Retrofit
    @Inject
    @Named("forToken")
    lateinit var retrofitForToken: Retrofit

    val fsUser = FSUser()

    init {

        MyApp.graphRepository.inject(this)
    }

    override fun login(userName: String, password: String, onFinish: (Boolean) -> Unit): Unit {
        AsyncTask.execute {
            try {
                if (MyOptions.Firebase.GET()!!) {
                    fsUser.login(userName, password, onFinish)
                } else
                    if (MyOptions.Online.GET()!!) {
                        val clientForToken: APIUser = retrofitForToken.create(APIUser::class.java)
                        val call: Call<DMAccessToken> = clientForToken.Token("password", userName, password)

                        val tokenResponse: Response<DMAccessToken> = call.execute()
                        if (tokenResponse.isSuccessful) {
                            MyOptions.Token.SET(tokenResponse.body()!!.access_token)
                            MyOptions.RefreshToken.SET(tokenResponse.body()!!.refresh_token)
                            MyOptions.User.SET(DMLoggedUser("", tokenResponse.body()!!.UserID, "", tokenResponse.body()!!.RoleID, tokenResponse.body()!!.RoleName, tokenResponse.body()!!.UserName))

                            val httpBuilder = OkHttpClient.Builder()
                            val authenticator = TokenAuthenticator(retrofitForToken)
                            httpBuilder.authenticator(authenticator)
                            httpBuilder.addInterceptor(Interceptor { chain: Interceptor.Chain? ->
                                if (chain != null) {
                                    val request: Request = chain.request()
                                    val addHeader = request.newBuilder().addHeader("Authorization", "Bearer " + MyOptions.Token.GET())
                                    return@Interceptor chain.proceed(addHeader.build())
                                }
                                return@Interceptor null
                            })

                            retrofit = Retrofit.Builder()
                                    .baseUrl(MyOptions.IP)
                                    .client(httpBuilder.build())
                                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                                    .addConverterFactory(GsonConverterFactory.create()).build()
                            onFinish(true)
                        } else
                            onFinish(false)
                    } else {
                        MyOptions.User.SET(dao.login(userName, password)!!)
                        onFinish(true)
                    }
            } catch (e: Exception) {
                onFinish(false)
            }
        }
    }

    override fun select(onSuccess: (List<DMUser>) -> Unit): Unit {
        AsyncTask.execute({
            onSuccess(dao.select())
        })
    }

    override fun insertUser(user: DMUser) {
        AsyncTask.execute({
            dao.insertUser(EntityUser(user))
        })

    }

    override fun insertRole(role: DMRole) {
        AsyncTask.execute({
            dao.insertRole(EntityRole(role))
        })
    }
}