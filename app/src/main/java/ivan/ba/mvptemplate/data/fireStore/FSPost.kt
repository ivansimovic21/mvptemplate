package ivan.ba.mvptemplate.data.fireStore

import com.google.firebase.firestore.FirebaseFirestore
import ivan.ba.mvptemplate.data.datamodels.DMPost
import ivan.ba.mvptemplate.data.datamodels.DMUser
import ivan.ba.mvptemplate.data.fireStoreInterface.FSPostInterface
import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepPostInterface
import ivan.ba.mvptemplate.data.viewmodels.posts.VMPostListItem

class FSPost : FSPostInterface {


    val myDB = FirebaseFirestore.getInstance()
    val collectionPosts = myDB.collection("posts")


    override fun getPosts(onFinish: (List<VMPostListItem>?) -> Unit) {
        val list = ArrayList<VMPostListItem>()
        collectionPosts.get().addOnSuccessListener {

            it.forEach {
                list.add(VMPostListItem(it.id, 0, it.getString("userId")!!, 0, it.getString("userName")!!, it.getString("title")!!, it.getString("text")!!))
            }
            onFinish(list)
        }
    }

    override fun getPostByID(postID: String, onFinish: (VMPostListItem?) -> Unit) {
        collectionPosts.document(postID).get().addOnSuccessListener {
            if (it.exists()) {
                val item = VMPostListItem(it.id, 0, it.getString("userId")!!, 0, it.getString("userName")!!, it.getString("title")!!, it.getString("text")!!)
                onFinish(item)
            }
        }
    }

    override fun insertPost(userID: String, userName: String, title: String, text: String, onFinish: (Long?, String?) -> Unit) {
        collectionPosts.add(mapOf(
                "userId" to userID,
                "userName" to userName,
                "title" to title,
                "text" to text
        )).addOnSuccessListener {
            onFinish(null, it.id)
        }.addOnFailureListener {
            onFinish(null, null)
        }
    }

    override fun updatePost(id: String, userID: String, userName: String, title: String, text: String, onFinish: (Boolean) -> Unit) {
        collectionPosts.document(id).set(mapOf(
                "userId" to userID,
                "userName" to userName,
                "title" to title,
                "text" to text
        )).addOnSuccessListener {
            onFinish(true)
        }.addOnFailureListener {
            onFinish(false)
        }
    }

    override fun deletePostByID(id: String, onFinish: (Boolean) -> Unit) {
        collectionPosts.document(id).delete().addOnSuccessListener {
            onFinish(true)
        }.addOnFailureListener {
            onFinish(false)
        }
    }

}