package ivan.ba.mvptemplate.data.fireStore

import com.google.firebase.firestore.FirebaseFirestore
import ivan.ba.mvptemplate.data.datamodels.DMLoggedUser
import ivan.ba.mvptemplate.data.datamodels.DMRole
import ivan.ba.mvptemplate.data.datamodels.DMUser
import ivan.ba.mvptemplate.data.fireStoreInterface.FSUserInterface
import ivan.ba.mvptemplate.data.reposetoryInterfaces.RepUserInterface
import ivan.ba.mvptemplate.util.options.MyOptions

class FSUser : FSUserInterface {

    val myDB = FirebaseFirestore.getInstance()
    val collectionUsers = myDB.collection("users")
    val collectionRoles = myDB.collection("roles")

    override fun login(userName: String, password: String, onFinish: (Boolean) -> Unit) {
        collectionUsers.whereEqualTo("userName", userName).whereEqualTo("password", password).get().addOnSuccessListener {
            try {
                it.forEach {
                    MyOptions.User.SET(DMLoggedUser(it.id, 0, it.getString("roleId")!!, 0, it.getString("roleName")!!, it.getString("userName")!!))
                }
                onFinish(true)
            } catch (E: Exception) {
                onFinish(false)
            }
        }.addOnFailureListener({
            onFinish(false)
        })
    }

    override fun select(onSuccess: (List<DMUser>) -> Unit) {
        val list = ArrayList<DMUser>()
        collectionUsers.get().addOnSuccessListener {

            it.forEach {
                list.add(DMUser(it.getString("id")!!, 0, it.getString("roleId")!!, 0, it.getString("roleName")!!, "", it.getString("userName")!!, it.getString("password")!!))
            }
            onSuccess(list)
        }
    }

}