package ivan.ba.mvptemplate.inject

import dagger.Component
import ivan.ba.mvptemplate.data.reposetory.RepPost
import ivan.ba.mvptemplate.data.reposetory.RepUser
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(RepositoryModule::class))
interface RepositoryComponent {
    fun inject(target: RepPost)
    fun inject(target: RepUser)
}