package ivan.ba.mvptemplate.inject

import android.app.Application
import dagger.Module
import dagger.Provides
import ivan.ba.mvptemplate.data.SQLite.MyDatabase
import ivan.ba.mvptemplate.data.SQLite.dao.DAOPost
import ivan.ba.mvptemplate.data.SQLite.dao.DAOUser
import ivan.ba.mvptemplate.util.options.MyOptions
import ivan.ba.mvptemplate.util.retrofit.TokenAuthenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class RepositoryModule(val application: Application) {

    @Provides
    @Singleton
    fun provideDAOUser(): DAOUser {
        val database = MyDatabase.getInstance(application)
        return database.userDao()
    }

    @Provides
    @Singleton
    fun provideDAOPost(): DAOPost {
        val database = MyDatabase.getInstance(application)
        return database.postDao()
    }

    @Provides
    @Named("forToken")
    @Singleton
    fun provideRetrofitForToken(): Retrofit {
        return Retrofit.Builder().baseUrl(MyOptions.IP).addConverterFactory(GsonConverterFactory.create()).build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val httpBuilder = OkHttpClient.Builder()
        val retrofitForAuthentication = Retrofit.Builder().baseUrl(MyOptions.IP).addConverterFactory(GsonConverterFactory.create()).build()
        val authenticator = TokenAuthenticator(retrofitForAuthentication)
        httpBuilder.authenticator(authenticator)
        httpBuilder.addInterceptor(Interceptor { chain: Interceptor.Chain? ->
            if (chain != null) {
                val request: Request = chain.request()
                val addHeader = request.newBuilder().addHeader("Authorization", "Bearer " + MyOptions.Token.GET())
                return@Interceptor chain.proceed(addHeader.build())
            }
            return@Interceptor null
        })

        val retrofit = Retrofit.Builder()
                .baseUrl(MyOptions.IP)
                .client(httpBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).build()

        return retrofit
    }

}