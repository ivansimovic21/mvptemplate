package ivan.ba.mvptemplate.inject

import dagger.Component
import ivan.ba.mvptemplate.view.areaPost.PostModule
import ivan.ba.mvptemplate.view.areaPost.postAdd.PostAddActivity
import ivan.ba.mvptemplate.view.areaPost.postDetails.PostDetailsActivity
import ivan.ba.mvptemplate.view.areaPost.postList.PostListActivity
import ivan.ba.mvptemplate.view.areaPost.postUpdate.PostUpdateActivity
import ivan.ba.mvptemplate.view.login.LoginActivity
import ivan.ba.mvptemplate.view.login.LoginModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, LoginModule::class, PostModule::class))
interface ApplicationComponent {

    fun inject(target: LoginActivity)
    fun inject(target: PostAddActivity)
    fun inject(target: PostDetailsActivity)
    fun inject(target: PostListActivity)
    fun inject(target: PostUpdateActivity)

}