package ivan.ba.mvptemplate.view.login

import com.nhaarman.mockito_kotlin.anyOrNull
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.never
import ivan.ba.mvptemplate.data.datamodels.DMUser
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import kotlin.test.assertEquals

class LoginPresenterTest {

    lateinit var mockLoginModel: LoginMVP.Model
    lateinit var mockLoginView: LoginMVP.Activity
    lateinit var presenter: LoginPresenter

    lateinit var user: DMUser

    @Before
    fun setup() {
        mockLoginModel = mock(LoginMVP.Model::class.java)

        user = DMUser(1, 1, "User", "User", "User", "User")

        mockLoginView = mock(LoginMVP.Activity::class.java)

        presenter = LoginPresenter(mockLoginModel)
        presenter.setUpView(mockLoginView)

    }

    @Test
    fun testSuccessfulLogin() {

        val callbackArgCaptor = argumentCaptor<(Boolean) -> Unit>()

        presenter.login()

        verify(mockLoginModel).login(anyOrNull(), anyOrNull(), callbackArgCaptor.capture())

        callbackArgCaptor.firstValue.invoke(true)
        assertEquals("",presenter.loginDataModel.userName.get())
        assertEquals("",presenter.loginDataModel.password.get())
        assertEquals(true,presenter.loginDataModel.loginEnabled.get())
        verify(mockLoginView).startPostsActivity()
        verify(mockLoginView, never()).showFailedLogin()
    }

    @Test
    fun testFailedLogin() {

        val callbackArgCaptor = argumentCaptor<(Boolean) -> Unit>()

        presenter.login()

        verify(mockLoginModel).login(anyOrNull(), anyOrNull(), callbackArgCaptor.capture())

        callbackArgCaptor.firstValue.invoke(false)
        assertEquals("",presenter.loginDataModel.userName.get())
        assertEquals("",presenter.loginDataModel.password.get())
        assertEquals(true,presenter.loginDataModel.loginEnabled.get())

        verify(mockLoginView, never()).startPostsActivity()
        verify(mockLoginView).showFailedLogin()
    }

    @Test
    fun testOpenDialog() {

        presenter.handleHelpClick()
        verify(mockLoginView).openHelpDialog()

    }
}