package ivan.ba.mvptemplate.view.login

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

/**
 * Created by isimovic on 31.01.2018..
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = intArrayOf(21))
class LoginActivityTest {

    var activity:LoginActivity? = null

    @Before
    fun setUp() {
        activity = Robolectric.buildActivity(LoginActivity::class.java).create().start().resume().visible().get()
    }

    @After
    fun tearDown() {
        activity?.finish()
    }

    @Test
    fun startPostsActivity() {
        activity?.onClickLogin()
    }
}